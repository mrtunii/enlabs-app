# WebAPI for transactions

## Task
Possible states (win, lost):
1. Win requests must increase the user balance
2. Lost requests must decrease user balance.
3. Each request (with the same transaction id) must be processed only once.
4. You should know that account balance can't be in a negative value.
5. The application must be competitive ability. 

## Used Packages

* [gocron](github.com/jasonlvhit/gocron) - For scheduling and running jobs.
* [gorm](github.com/jinzhu/gorm) - ORM for database interaction
* [godotenv](github.com/joho/godotenv) - For loading variables from .env file
* [decimal](github.com/shopspring/decimal) - For interaction with prices
* [pq](github.com/lib/pq) - postgres driver
* [mux](github.com/gorilla/mux) - For routing


## How to run

1. Clone repository
2. use `cp .env.example .env` command to copy env file and change database variables in it.
3. `go build main.go`
4. `go run main.go` (This will take care of database migrations and seeding, if it's first run)

## Access the api
***By default API will be on http://localhost:8000 url***

***You can import postman collection named: "ENLABS.postman_collection.json" There is endpoint and test data to play with.***


