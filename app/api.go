package app

import (
	"ENLABS/app/handlers"
	"github.com/gorilla/mux"
)

//NewRouter is main routing entry point
func NewRouter() *mux.Router {
	r := mux.NewRouter()

	handlers.TransactionHandler(r)

	return r
}

