package database

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/joho/godotenv"
	"os"
)

var db *gorm.DB
var err error

func Open() (*gorm.DB, error) {

	err := godotenv.Load()
	username := os.Getenv("DB_USER")
	password := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")
	dbHost := os.Getenv("DB_HOST")

	dbUri := fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable password=%s", dbHost, username, dbName, password)

	db, err = gorm.Open("postgres", dbUri)

	if err != nil {
		return nil,err
	}

	return db,nil
}

func Close() error {
	return db.Close()
}