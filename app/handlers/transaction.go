package handlers

import (
	database "ENLABS/app/config"
	"ENLABS/app/models"
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/shopspring/decimal"
	"net/http"
)

func TransactionHandler(r *mux.Router) {
	r.HandleFunc("/api/transactions", createTransactionHandler).Methods("POST")
	r.HandleFunc("/api/transactions", testTransaction).Methods("GET")
}

func testTransaction(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(200)
	w.Write([]byte("OK"))
}

func createTransactionHandler(w http.ResponseWriter, r *http.Request) {

	db, _ := database.Open()
	w.Header().Set("Content-Type", "application/json")
	var transaction models.Transaction
	_ = json.NewDecoder(r.Body).Decode(&transaction)

	transaction.Source = r.Header.Get("Source-Type")

	// If we already processed transaction with same transaction id, return error.
	var existingTransaction models.Transaction
	if !db.Unscoped().Where("transaction_id = ?", transaction.TransactionId).First(&existingTransaction).RecordNotFound() {
		error := models.Error{Error: "Transaction with ID: '" + transaction.TransactionId + "' already exists !"}
		w.WriteHeader(400)
		json.NewEncoder(w).Encode(error)
		return
	}

	// If we don't have source type in database, return error.
	var existingSourceType models.SourceType
	if db.Where("name = ?", transaction.Source).First(&existingSourceType).RecordNotFound() {
		error := models.Error{Error: "Invalid source type"}
		w.WriteHeader(400)
		json.NewEncoder(w).Encode(error)
		return
	}

	db.Create(&transaction)

	// Update user balance
	var balance models.Balance
	db.First(&balance)
	switch transaction.State {
	case "win":
		balance.Amount = balance.Amount.Add(transaction.Amount)
	case "lost":
		balance.Amount = balance.Amount.Sub(transaction.Amount)
		// if balance amount is less then 0, then we are updating it to 0 because it can't be less than 0;
		if balance.Amount.LessThan(decimal.NewFromInt(0)) {
			balance.Amount = decimal.NewFromInt(0)
		}
	default:
		error := models.Error{Error: "Invalid State"}
		w.WriteHeader(400)
		json.NewEncoder(w).Encode(error)
		db.Delete(&transaction)
		return
	}

	db.Save(&balance)

	db.Close()

	json.NewEncoder(w).Encode(transaction)
}
