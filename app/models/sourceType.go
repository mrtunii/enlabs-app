package models

import "github.com/jinzhu/gorm"

type SourceType struct {
	gorm.Model
	Name string
}