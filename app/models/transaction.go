package models

import (
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
)

type Transaction struct {
	gorm.Model
	State string `json:"state"`
	Amount decimal.Decimal`json:"amount"  gorm:"type:numeric"`
	TransactionId string `json:"transactionId"`
	Source string
}
