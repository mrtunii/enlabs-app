package main

import (
	"ENLABS/app"
	"ENLABS/app/config"
	"ENLABS/app/models"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"github.com/shopspring/decimal"
	"log"
	"net/http"
	"os"
)

var err error

func main() {
	// load env variables
	err := godotenv.Load()

	// Load routes
	router := app.NewRouter()

	fmt.Printf("Attempting to connect to database\n")

	db, err := database.Open()

	if err != nil {
		panic("Failed to connect database\n")
	} else {
		fmt.Printf("Done !\n")
	}

	fmt.Printf("Running Migrations !\n")

	db.AutoMigrate(&models.Transaction{})
	db.AutoMigrate(&models.Balance{})
	db.AutoMigrate(&models.SourceType{})

	fmt.Printf("Done !\n")

	seedBalanceData(db)
	seedSourceTypes(db)

	database.Close()

	fmt.Printf("\n")
	fmt.Printf("Application Started on port:" + os.Getenv("APP_PORT") + " !\n")
	log.Fatal(http.ListenAndServe(":"+os.Getenv("APP_PORT"), router))
}

/**
Because there is no users
we need only 1 item in balances table.
so this will check if there is data in table, if not creating with amount = 0
*/
func seedBalanceData(db *gorm.DB) {

	fmt.Printf("Running Balance Data Seed !\n")
	var balanceItemsCount int
	db.Model(&models.Balance{}).Count(&balanceItemsCount)

	if balanceItemsCount == 0 {
		balance := models.Balance{Amount: decimal.NewFromInt(0)}

		db.Create(&balance)


	}
	fmt.Printf("Done !\n")
}

/**
	For now we have only 3 types of sources.
	I decided to keep them in the database because there may be other types as well.
	So this seeds only game,server and payment type to database.
 */
func seedSourceTypes(db *gorm.DB)  {

	fmt.Printf("Running Source Type Seed !\n")

	sourceTypesData := [3]string {"game", "server", "payment"}

	var sourceTypeItemsCount int
	db.Model(&models.SourceType{}).Count(&sourceTypeItemsCount)

	if sourceTypeItemsCount == 0 {
		for _,srcType := range sourceTypesData {
			newSourceType := models.SourceType{Name:srcType}
			db.Create(&newSourceType)
		}
	}

	fmt.Printf("Done !\n")
}
